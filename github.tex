\section{Integrating Autotests in GitHub for Automated Grading}
\label{sect:github}

Albeit a prerequisite for automated grading, autotests respectively the
capability of writing and running unit tests does not suffice for automated
grading. Autotests already give immediate feedback to students so they know
whether they are on track for completing any given programming assignment. They
also give hints as to where mistakes were made. However, they do not provide
lecturers with the ability to obtain a spreadsheet with the grades of a class
of students. They also do not give students a way to file their assignment work
and teachers to efficiently download all assignments at once.

Currently, the most widespread solution providing these missing features
is GitHub Classroom. It is a platform that integrates git, automated
tests and classroom management and grading features by assigning points for
tests. This section will therefore explain
how to use GitHub Classroom efficiently with the aforementioned testing
frameworks to facilitate the use of automated grading for fellow teachers.

Resorting to any git based solution for grading has the additional advantage of
introducing students to git, the industry's favorite version control system
which is in high demand in the labor market. Students are also experiencing the
benefits of automated testing which is yet another important skill that they
should have.

Despite all the praise, Github Classroom is far from perfect and will certainly
benefit from concurrence. The associated tools are not in a stable state with
features being added and removed and adjustments to the inner workings
regularly happening. For example, it is not possible to hide the tests used
for grading from students. This means that students can read which exact
outcomes each test expects for a given input and write a solution limited to
return these exact values. Or students could simply overwrite a teacher's
definition for determining a grade effectively returning full points without
running any code. While being possible, in the experience of the author,
these kinds of cheating rarely happen. Nevertheless, oral examinations of
students' skills are a critical addition to automated grading.
This is to ensure that assignments have been solved by each individual student
themselves within the limits set for each assignment. For example, students
may be allowed to use websites like stackoverflow and regular search engines
when solving non-trivial assignments, but be restricted from using artificial
intelligence.

Since tests cannot be properly hidden from students, a key recommendation is
hence to allow and encurage students
to look at the tests. Tests serve as additional specification and the ability
to understand the details of a task by reading a set of automated tests is an
important asset. This avoids technical difficulties and does not attempt to
play cheap tricks on students.

To avoid cheating the tests, review random
samples of their assignments (ideally in front of the class as a collective
code review) and make sure to orally confirm that students completed their
assignments themselves. Also, albeit the possibility of placing test cases
in a designated folder, as stated in subsection \ref{ssect:test-files},
this is not a practice recommended by the author. Put the tests as
close to the source as possible. Particularly in case of Python: don't place
tests in any subdirectory as relative inputs. This might work well in your
environment but may cause massive problems on students' computers and in
the Linux containers used by Githue Classroom.


\subsection{Getting Started with Github Classroom}

The use of Github Classroom is currently free of charge and it is not to be
expected that this will change. However, given the possibility of abusing the
massive computing resources provided for Classroom accounts, only accredited
schools are allowed to use the system. Probably the most prominent possibility
for misuse would be to use the provided computational power for mining
crypto currencies.

Individual lecturers can sign up directly using their regular GitHub account.
The URL for signing up is \url{https://classroom.github.com/}. For receiving
more features, schools can enroll in the
GitHub Campus Program\footnote{(\url{https://education.github.com/schools}}.
For example, HTL Bulme is
part of this program. The author of this publication is one of the
administrators of the Bulme organization on GitHub.

\noindent{
\includegraphics[width=0.99\linewidth]{images/bulme_github.png}
\captionof{figure}{Institutions using GitHub Campus around Graz on 2024-01-02}
}

Educational institution's GitHub administrators are encuraged to collaborate.
For example, when Bulme joined the GitHub Campus Program, Bulme teachers
thankfully received training by the GitHub administrator of HTL Weiz.

Once signed in to the web interface provided by GitHub Classroom, lecturers
can create classrooms. To add students to a GitHub Classroom, it is possible to
import rosters from learning management systems like Google Classroom or
Moodle. However, it may be easier to upload a comma-separated values (CSV) or
text file with the
students' names. As soon as a classroom is created, it is possible to define
assignments. When defining assignments, lecturers can add tests in a rather
cumbersome way.
Screenshots of the process are omitted here as it is
rather self explanatory and the web interface is potentially subject to
changes. Instead of setting up tests on the web interface, they can also
be specified more efficiently by editing textual configuration files.

When an assignment is created, it is possible to provide a git repository
that will be the basis when students start working on the assignment. Each
student will receive a fork of this repository. The best way to configure
automated grading is hence to add all the required autotest files and
the configuration file for automated grading to this repository.
The ever expanding documentation of GitHub Classroom is available online at
\url{https://docs.github.com/en/education}.


\subsection{Effienctly Defining Criteria for Grading}
Teachers can prepare all necessary files for an assignment in a designated
repository. These files may include required input files, code stubs,
partial definitions of functions that students have to complete or even a
large amount of source code that is to be extended by students. In addition,
the repository should contain all automated tests (see section
\ref{sect:autotests} for an introduction on creating them). Finally, the
required configuration for grading should be added essentially telling GitHub
Classroom how many points students will receive for each test.

An example for setting up such a repository (however already containing the
solutions as well) is provided at
\url{https://github.com/senarclens/automated_grading}. To enable or adjust
rules for automated grading, the JSON configuration file
\texttt{.github/classroom/autograding.json} has to be edited.
In order for this file to be applied when students push their changes, the
GitHub Classroom provided file \texttt{.github/workflows/classroom.yml} has to
be in-place. An example of \texttt{autograding.json} for executing
test-runners written in C, C++ and Python is listed below.

\lstinputlisting{code/basic_autograding.json}

The example above shows an array with the key \texttt{"tests"}. It has three
elements - one for each test scripts being executed. The important elements
are

\begin{description}
  \item[name] name to identify the test in the logs
  \item[setup] optional commands to run before tests
  \item[run] command to start the test
  \item[timeout] maximum duration in minutes before failing
  \item[points] number of points awarded for the test
\end{description}

There are other elements that can be used allowing to test for specific inputs
and outputs \citep{GitHub:UseAutograding}. However, these other elements serve
little purpose for those
capable of harnessing the power of automated tests.

The definition for grading the Python example sets 5 points for this task.
It will timeout after 0.3 minutes (18 seconds) rewarding 0 points to student
submissions taking longer. Unfortunately, at the time of this writing,
the timeout includes the time required for the setup. Note that, at the time of
this writing, neither the information that fractional values for the timeout
are possible nor the information that the timeout includes the time for the
setup were available in the official documentation. As the compilation of
a framework can take very long, the value of the optional timeout feature is
limited. Make sure to always set the timeout by far large enough to avoid
flaky tests, i.e. tests that occasionally pass and fail without the code
changing. Depending on the load of the servers running the tasks, execution
time may vary considerably. To focus on the performance of student submissions,
it is best to avoid any activity in the setup that takes too long.

Before the test starts, the line
\texttt{cd py; sudo apt install python3-pytest} is executed. Since the Python
code in the repository resides in a folder called \texttt{py}, the folder needs
to be accessed. As explained in subsection \ref{ssect:pytest} on page
\pageref{ssect:pytest}, the test runner must be installed via
\texttt{sudo apt install python3-pytest} which is the easiest way to do so
on Debian-based Linux platforms like the default Linux containers provided by
Github. Finally, the actual test is executed by issuing the \texttt{"run"}
command \texttt{python -m pytest}. Like all introduced test runners, this
command returns the status code \texttt{0} (success) if and only if no tests
fail. In all other cases, a non-zero status is returned. GitHub Classroom
interprets the status code and rewards all points of the task to students if
their submission passed all the tests executed by this command. Hence, a single
failing test results in 0 points for the student, even if the assignment is
almost perfect.

Given that there are three tasks in the \texttt{autograding.json} file above,
students can obtain 0, 5, 10 or 15 points. That allows for two differnt passing
grades (10 and 15 points) - not enough for most grading systems. It is hence
often a good idea to split up tests into smaller units. The next subsection
shows how this can be achieved with \texttt{pytest}.


\subsection{Split up Points for a Small Assignment}
Since the introduced test runners (Catch2, pytest and Criterion) all support
filtering which tests are executed, it is straight-forward to split tests
into smaller units. For example, the test for the python implementation
contains two separate test cases. These can be listed via

\begin{lstlisting}
$ python -m pytest --collect-only
====================== test session starts ======================
platform linux -- Python 3.11.6, pytest-7.4.0, pluggy-1.2.0
rootdir: /home/gerald/repos/github/automated_grading/py
collected 2 items

<Module sum_to_test.py>
<Function test_sum_to>
<Function test_sum_to_slow>

================== 2 tests collected in 0.00s ===================
\end{lstlisting}

With this knowledge, it is straight-forward to improve
\texttt{autograding.json}. For example, one could award 3 points for the
correct result and another 2 points if the performance is fast enough and
all edge cases pass as well.
The adjusted part of the file now looks like

\begin{lstlisting}
    {
    "name": "Test Python implementation for smaller inputs",
      "setup": "cd py; sudo apt install python3-pytest",
      "run": "python -m pytest -k 'not slow'",
      "timeout": 0.3,
      "points": 3
    },
    {
      "name": "Test Python implementation for very large inputs",
      "setup": "cd py; sudo apt install python3-pytest",
      "run": "python -m pytest -k 'slow'",
      "timeout": 0.3,
      "points": 2
      }
\end{lstlisting}

With this small modification in place, grading can be made much more
fine-grained. Interested readers could split the tests even further as an
exercise. A larger number of smaller auto tests with fewer assertions can
also improve the clarity of the feedback given to students.
The complete \texttt{autograding.json} with all autotests being split into
smaller tests is listed in the next section.


\subsection{Temporarily Deactivating Tests}
Sometimes it is necessary to temporarily remove a test. Simply deleting it
is frequently the best option since the use of version control allows to
selectively undo the deletion at any time. However, it may still sometimes
be desirable to comment out a test. Unfortunately, the \texttt{json} standard
does not offer a feature for commenting out code. The easiest way to achieve
something similar would be to add a key to the \texttt{json} file that is
simply ignored. The example below shows how this can be done by adding a
section for deactivated tests. This section could, for example, contain
linters (tools to analyze and identify potential problems in source code)
until all problems regarding their setup and configuration is complete.

The listing below contains deactivated tests for code quality in the C++
language. Of course, similar tools also exist for C and Python. It shows a
complete version of \texttt{autograding.json} after splitting the tests
allowing for more fine-grained grades.

\lstinputlisting{code/complete_autograding.json}



\subsection{Autograding Logs and Obtaining Grades}
Whenever possible, both students and teachers should run their tests locally.
However, for automated grading it is required to have the tests run in GitHub's
Linux containers. In case anything unexpected happens, a detailed log of all
events is made available for each run. These logs easily contain hundreds of
lines of information. For debugging purposes, teachers can even log
additional information. The below illustration shows a small
excerpt of such a log.

\noindent{
\includegraphics[width=0.99\linewidth]{images/github_actions_log.png}
\captionof{figure}{Small excerpt of a log provided by GitHub actions}
}

The logs are available for both students and teachers to see the output of
potentially failing tests or issues with the installation of any requirements.
They can be directly accessed from each student's repository by clicking on
the green checkmark (if everything went well) or red cross (if something went
wrong) in the header of the repository's file listing. The logs are also
accessible from the teacher's overview of any assignment (see the checkmarks
and crosses in the screenshot below). From this overview, teachers may also
download a CSV file with all the grades that also maps the student's github
user names to their rostered real names. Finally, from the same overview, it
is also possible to download all student repositories at once for manual
investigation.

\begin{center}
  \includegraphics[width=0.49\linewidth]{images/download_grades.png}
  \captionof{figure}{Teacher's overview of a real life assignment}
\end{center}

Of course, there are many more features to discover on the platform. These
include so-called feedback pull requests comparable to a similar feature on
Exercism. They allow to give individual feedback to (a subset of) the lines of
code written by a student. The goal of this chapter solely is an introduction
for fellow teachers to get a foothold in the topic of automated grading with
GitHub Classroom.
