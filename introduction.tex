\section{Introduction}
\label{sect:introduction}

In today's technology-driven world, programming has become an essential skill
for individuals seeking employment in various fields. However, assessing
programming assignments manually can be a time-consuming and tedious task for
educators. This has led to the development of automated grading systems, which
aim to streamline the grading process and provide swift, more objective
feedback to students.


\subsection{Rationale for Choosing the Topic}

Automated grading is a promising approach for enhancing programming education.
Available tools offer several advantages over manual grading.

\begin{description}
\item[Feedback:]
automated grading systems can provide detailed and immediate feedback to
students, helping them identify and correct mistakes promptly.
Furthermore, students can automatically receive useful hints on current issues.

\item[Efficiency:]
automated grading can significantly reduce the time required for assessment,
allowing educators to focus on other aspects of teaching and providing more
personalized feedback to students.

\item[Consistency:]
automated grading ensures consistent evaluation of student work, minimizing the
impact of human subjectivity and bias.

\item[Accessibility and Transparency:]
automated grading can be integrated with online learning platforms
(like GitHub Classroom), making grading information transparently accessible
to instructors and students. The necessary automated tests also serve as an
additional explanation on what the teachers actually expect and thus
facilitate communication of desired outcomes.
\end{description}

However, there is a large number of available tools, each posing
different technical challanges. Hence, the technologies enabling automated
grading can be overwhelming even for highly skilled teachers.

Therefore, the main objective of this bachelor thesis is to provide an
overview and serve as a hands on tutorial introduction for fellow
teachers.
To really benefit from the introduced technologies, a large pool of exercises
and automated tests is required. This can only be achieved by fostering
collaboration among computer science teachers. To enable synergies, the goal
of this bachelor thesis is the creation of learning material for fellow
computer science teachers to easier adopt the presented technologies.

Ethical considerations in using automated grading are beyond the scope of this
thesis.


\subsection{Definitions}

Throughout this thesis, a few key terms will be used.
For easier comprehension of the subsequent sections, here's a brief explanation
of these terms.

\begin{description}
  \item[Application programming interface (API)] allows
    different computer programs to communicate and share data with each other.
    It is like a specification that tells programs how to connect and interact
    with each other.

  \item[Distributed version control (DVC)] is a form of version control in
    which the complete source code, including its change history, is mirrored
    on every developer's computer. Compared to centralized version control,
    DVC speeds up most operations and improves the ability to work
    offline \citep{ChaconAndStraub:2023}.

  \item[Git] is the world's most popular DVC. No other technology is as widely
    used as Git. According to \citet{StackOverflow:2022}, 96.65\% of
    professional developers used Git in 2022.

  \item[Unit test:] is an automated test case, normally written and executed by
    software developers to ensure that a section of an application
    (known as the "unit") meets its design and behaves as intended
    \citep{HofmannEtAl:2020}.
\end{description}


\subsection{Overview of the Status Quo}
The author of this thesis is currently affiliated with both Univeristy of Graz
and HTL Bulme (an advanced vocational school in Graz, Austria). As observed by
the author, in neither of these institutions, automated grading of programming
assignments is systematically persued. The computer science teaching staff in
both organizations is using a variety of different teaching methods, tools and
programming languages and there is overall very limited collaboration when it
comes to sharing programming assignments.

In order to make this paper relevant, it should present currently highly
popular programming languages.
According to the Tiobe Index for December
2023\footnote{\url{https://www.tiobe.com/tiobe-index/}} as well as the Open Hub
Language Comparison\footnote{\url{https://openhub.net/languages/compare}},
the three most popular programming
languages at the moment are Python, C and C++.
Hence, these three languages will be used for
all practical examples in the rest of this thesis. Given the overall freedom of
teaching and the continuously evolving field of computer science, no curriculum
prescribes which programming languages ought to be tought. Hence, lecturers
are free to chose any of the above languages. Fortunately, all of these three
programming languages are already tought at HTL Bulme.



\subsection{Considerations from Learning Theory}

By adopting automated grading practices, educators can create a more effective
and student-centered learning environment for programming education.
For example, \citet{ShermanEtAl:2013} observed that students, when using an
automated feedback system, make more submissions per assignment, indicating
that they were leveraging feedback to improve their programs.
Considering the collateral damage of school closures and distance learning,
this additional feedback is particularly valuable. School closures led to a
larger spread in student abilities and prior knowledge.
This demands additional individual
assistance which is facilitated by automated grading in two ways.
Automated grading provides direct feedback to students and it potentially
frees teacher resources that can be used for enhanced individual feedback.

According to the principles of constructivism, effective learning occurs when
students construct their own understanding of a concept. Feedback plays a
crucial role in this process, as it provides students with information about
their progress and helps them identify areas for improvement. Automated grading
systems can provide students with immediate and detailed feedback on their code,
which can be beneficial for their learning. However, it is important to ensure
that the feedback provided is clear, actionable, and tailored to the individual
student's needs \citep{HattieAndTimperley:2007}.

When using automated grading systems, it is important that they are not overly
complex or difficult to use. If students find the system difficult to navigate
or understand, it can actually increase their cognitive load and hinder their
learning \citep{PaasEtAl:2010}. The tools proposed in this paper all increase
the cognitive load of students. It is hence important to introduce them
separately from any concepts of programming to reduce the cognitive
load. The use of the git version control system for automated assignment
grading therfore has both positive and negative aspects. Its complexity makes
it hard to really understand, but its omnipresence make it a tool that
software engineers have to learn regardless of its use for automated grading.
The key takeaway is that the tooling for
automated assessment has to be either easy to use or introduced separately
from any new programming concepts.

While automated grading can provide valuable feedback to students, it is
important to recognize that it is not a substitute for human judgment and
pedagogical expertise. Teachers still play a crucial role in interpreting
student work, especially considering the advent of artificial intelligence.
Students may be tempted to copy assignment solutions from previous years,
have artificial intelligence complete their assignment work or literally employ
ghost writers. Given all these options, additional oral assessment of the
automatically graded work is vital. Furthermore, personalized feedback
best meets the individual needs of students. Automated grading
systems can complement and support the work of teachers, but they should not be
seen as a replacement for human interaction and guidance.

These are just a few of the considerations from learning theory that ought to
be taken into account when starting to use automated grading systems for
programming assignments. Educators shall carefully consider such factors
to ensure that automated grading is used in a way that enhances student
learning.
