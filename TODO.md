Downsides: easy cheating when students want to cheat - manual controls are
required. But - manual oral tests are required anyway in times of AI and
ghostwriters.

# pytest - having tests in a hidden subdirectory
hiding autograding tests is not a good idea; it creates trouble for the
testrunner:

The "ImportError: attempted relative import with no known parent package" error occurs when Python is unable to find the parent package of a module that is attempting to perform a relative import. This can happen when the module is located in a directory that starts with a dot (.), as Python interprets such directories as hidden and excludes them from its package search path.

Even though you have an __init__.py file in both the dot-prefixed directory and its parent directory, Python is still unable to recognize the dot-prefixed directory as a package. This is because Python only considers directories containing __init__.py files as packages if they are explicitly included in the search path.

To resolve this issue, you have a few options:

    Rename the dot-prefixed directory: The simplest solution is to rename the directory to something that doesn't start with a dot. This will allow Python to recognize it as a package and resolve the import issues.

    Explicitly add the dot-prefixed directory to the search path: You can explicitly add the dot-prefixed directory to Python's search path using the sys.path.insert() method. This will allow Python to find the directory and its modules when performing relative imports.

- it doesn't provide additional value as talented but dishonest students will
still easily find the "hidden" tests

- if properly hidden tests would be required, the files would have to be
tested on a remote machine by using an adequate API (eg rest API) - with the
potential downside of having to run potentially harmful code on one of your
servers

bottom line - don't even try to hide the tests
