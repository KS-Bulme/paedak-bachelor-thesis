\section{Creation of Automated Tests}
\label{sect:autotests}

As laid out in the introduction, Python, C and C++ are all outstandingly popular
programming languages that are already tought at HTL Bulme. In this section,
novices to creating automated tests will learn how this can be done and which
considerations are necessary to do so. The creation of automated tests is
essentially a prerequisite to automated grading of programming assignments as
the sole alternative (testing for program I/O) is extremely limited and not
sufficient for anything but the most basic toy programs.

Automated software tests are not originally aimed for teaching but serve as
valuable tools for professional programmers. Hence, being able to read and
understand automated tests as well as to later on write them is a valuable
skill for computer science students. Hence, the benefits of automated tests
in teaching are not limited to assessment of programmign assignments.

To make the introduction to testing easier, the same small programming
assignment will be
tested for each of the above languages. Automated tests can be used for
testing small independent units of code. This has the advantage of allowing
students to track their progress in larger assignments. It also facilitates
objective grading of assignments in a non-binary way. Without the ability to
split up a task in smaller pieces, teachers might end up awarding only the
highest and lowest available grades. Students succeeding with an assignment
would receive the best grade, while others would receive the lowest grade
with no grades in the middle.

In order to write and execute unit tests, a so-called testing framework is
required. There are dozens of different testing frameworks for C and C++ and
a handful of frameworks for Python. An excellent overview of available
testing and mocking frameworks is provided in the
"List of unit testing frameworks" \citep{wiki:UnitTestingFrameworks}.
A decision on the introduced frameworks was made based upon their

\begin{enumerate}
  \item technical features
  \item software license
  \item popularity in programming assessment sites
  \item quality of their documentation
  \item ease of use and
  \item activity of development
\end{enumerate}

At the time of this writing, a reasonable choice of testing frameworks is
pytest (for Python), Criterion (for C) and Catch2 (for C++).

All created tests should be be written in a way to make them usable in other
learning platforms like Exercism as well. This offers multiple advantages.
One of them is the
ability to contribute to an internationally renowned open source programming
training project with very limited additional effort. Another critical benefit
is that teachers won't easily be trapped in a vendor lock in of a single
programming assessment software.

All of the source code in this section is
available using the permissive MIT open source license. It can be
downloaded or forked from the public repository provided at
\url{https://github.com/senarclens/automated_grading}.

Because computer scientists frequently prefer the command-line, the required
commands to perform actions described in this text will generally be provided.
This is a simple and concise way of communicating what needs to be done.
In addition, many of the introduced
commands are required when setting up assignments on GitHub classroom. Finally,
HTL Bulme provides a command-line only server for students allowing them to
work on their programming assignments
from home without installing any special tools on their machines.

The source code examples mentioned above can also be directly cloned using
the following git command.

\begin{lstlisting}
git clone git@github.com:senarclens/automated_grading.git
\end{lstlisting}


\subsection{Programming Assignment}
\label{ssect:assignment}
For the remainder of this section, a small introductory assignment will be
used: calculating a sum of consective integers. The task is to write a program
that implements the function \texttt{sum\_to(num)} which adds up all integers
between 0 and a user-defined upper limit \texttt{num},
i.e. \texttt{1 + 2 + 3 + ... + num}. The upper limit has to be integral and must
be included in the sum.

Lecturers must not forget to define both filenames to contain the solutions as
well as the exact API they expect the students to implement. This precision
is expedient as it brings students closer to a real life professional setting.
In this case, the file containing the program should be called
\texttt{sum\_to.\{c,cpp,py\}}.
It is usually beneficial if teachers provide some sample inputs and their
expected results in the task description. For students that are already capable
of reading unit tests, the autotests should be sufficient though as they also
serve as a part of the description.

The task at hand is small and easy to solve even for beginners when using
loops. It can also
be modified to become slightly more challenging. For example, students
can be told to only add numbers that can be divided by a given positive integer
without a remainder. The same task can also be used when teaching asymptotical
complexity as solving it with a loop is very inefficient. It is possible to
use separate automated test for correctness and performance. When testing the
solution for performance, students will have to devise or research an
efficient algorithm instead, but may be rewarded a part of the available points
for correctness.

When starting to create tests, teachers have to think about good test cases.
This is usually done using pencil and paper or brainstorming.
It is generally necessary to test one or more common cases and to also
include relevant edge cases. Awarding a fraction of the achievable points for
each passing test provides an objective non-binary result when grading.

For the example at hand, one could use \texttt{25} as a common case for the
upper limit. A good addition would be \texttt{26} to have at least one common
case of an even number for the upper limit. The edge cases for the problem at
hand usually include \texttt{0} (the smallest unsigned integer) and
\texttt{4294967295} (the largest 32 bit unsigned integer).
Expected results for these are \texttt{325}, \texttt{351}, \texttt{0} and
\texttt{9223372034707292160}.
In addition it would be good to also add, for example, \texttt{500000000} as
input (expected result \texttt{125000000250000000}) as \texttt{4294967295} does
not terminate at all when students fall for a common programming mistake.

\subsection{Considerations on Files Containing Tests}
\label{ssect:test-files}

There are many different standards dictating how to name files containing tests
and where to put them. One thing they all have in common is that consistency
is a must.

From the author's personal experience as software quality assurance engineer,
files containing tests should be as close as possible to the files they are
testing. Ideally, test files and implementation files should be placed
side-by-side in the same directory.

As a general recommendation for the filenames containing the
tests, it is beneficial to give them the same name as the implementation file,
followed by \texttt{\_test}. For example, in the case of the source code
residing in \texttt{sum\_to.\{c,cpp,py\}}, the tests should reside in
\texttt{sum\_to\_test.\{c,cpp,py\}}.
This offers the genuine advantage of test files and implementation files
being listed right next to the each other in file listings that are sorted by
name. This proximity makes it more likely that engineers keep the tests in sync
should the requirements for the implementation ever change.

\subsection{Testing Python Programs - pytest}
\label{ssect:pytest}

At the time of this writing, \texttt{pytest} is the unit testing framework and
test runner of choice when writing Python code.
A platform independent installation can be performed with the \texttt{pip}
tool.

\begin{lstlisting}
pip install -U pytest
\end{lstlisting}

On Debian-based flavors of Linux, it is usually best to perform the
installation using the distribution's advanced package tool \texttt{apt}
which is also directly available
in the Linux containers provided GitHub Actions. These Linux containers serve
for executing tests in order to perform automated grading in GitHub
Classroom. Note that at the time of this writing,
HTL Bulme already provides two fully operational lecture rooms with the current
version of Debian 12 (code name bookworm).

\begin{lstlisting}
sudo apt install python3-pytest
\end{lstlisting}

Once the installation is complete, \texttt{pytest} can be executed via

\begin{lstlisting}
python -m pytest
\end{lstlisting}

When the above command is issued, all tests in the current directory as well
as all contained subdirectories are executed and a nice, human readable
report is presented. When all executed tests are passing, the program returns
status 0 to the operating system indicating success. As soon as any test fails,
failure is indicated to the operating system. This fact is cruicial to
automatically creating grades via GitHub classroom.

If multiple tests are
available, \texttt{pytest} allows to list the tests and filter them to run
only a desired subset. The multitude of available help commands can be
consulted with a designated flag:

\begin{lstlisting}[language=bash]
python -m pytest --help  # get help
python -m pytest --collect-only  # list all available tests
# ignore tests with `slow` in their name
python -m pytest -k "not slow"
\end{lstlisting}

Given the impressive capabilities of the library, a comprehensive re-statement
of the official documentation is not beneficial in this paper. Instead, the
interested reader may consult the official documentation at
\url{https://docs.pytest.org}.

Aside of the very powerful automatic test discovery of the library, one of its
key strengths are the simplified assertions. Instead of forcing programmers
to learn a complicated set of pre-defined assertions, the library resorts
to Python's built-in \texttt{assert} keyword. This makes it much easier to
write test cases.

Below is a file listing implementing the test cases laid out in subsection
\ref{ssect:assignment}. It offers two distinct tests that can be executed
separately. No special programming skills are required. For any seasoned Python
programmer, reading and writing simple tests is self explanatory. Complex tests
that involve mocking or other advanced techniques are beyond the scope of this
paper.

\lstinputlisting[language=python]{code/sum_to_test.py}


\subsection{Testing C Programs - Criterion}
As there are very many concurring testing harnesses available for the C
language, there are multiple high quality options. Among the best is the
Criterion library, however other libraries like Unity (currently used by
Exercism) also have their distinct
advantages. Criterion is a relatively new library with a clean implementation.
It was written with the "Keep it simple, stupid!" (KISS) principle in mind,
making writing tests much easier.

It is, for example, used on Codewars (one of the proprietary platforms offering
programming exercises).
Criterion's authors have proven very responsive when in need of help or
suggesting
improvements (the author of this paper has been in contact with them on
multiple occasions). Using the MIT licence, it can be considered very
permissive open source software.
One of the most critical limitations is its dependence on C language macros.
However, as the use of macros is essentially a requirement due to limitations
in the C language itself, there is currently mature alternative testing library
written in C that does not rely on macros. It can only be hoped for this to
change in the future.

Criterion is already well integrated in recent versions of
Debian based Linux which is used for the Linux containers on GitHub classroom.
A local installation on any platform can be achieved by building the library
from source as
described in \url{https://criterion.readthedocs.io/en/master/setup.html}.
For readers fortunate enough to use a Linux or Unix based operating system,
the installation can be performed via their respective package manager. For
example, in Debian based distributions, the following command makes the
library available system-wide.

\begin{lstlisting}
sudo apt-get install libcriterion-dev
\end{lstlisting}

The library's source code is available in the projects git
repository\footnote{\url{https://github.com/Snaipe/Criterion/}}.
A series of samples for using it
efficiently are included there as well. Its documentation is available
at readthedocs\footnote{\url{https://criterion.readthedocs.io/}}.

A criterion-based implementation of the test cases defined in subsection
\ref{ssect:assignment} is in the listing below.

\lstinputlisting[language=c]{code/sum_to_test.c}

For a skilled C programmer, the above should be straight forward. The
assertions are a lot simpler since the library was refactored for its second
version. Each of the assertions are either based on \texttt{Criteria}
(pseudo-functions that evaluate to a boolean value) or
\texttt{TaggedCriteria} (special criteria that take an optional type tag as
their first argument). In the case above, the \texttt{eq}
\texttt{TaggedCriterion} is used. \texttt{eq} consists of a Tag, the actual
value returned by the tested function and the expected correct value.
Tags are a feature of Criterion. They represent a type that allows Criterion to
infer type information for better diagnostics when assertions fail.
\texttt{eq} tests the actual outcome for equality with the expected outcome.
Please note that when using Criterion, no boilerplate code is required. Not
even the main function has to be implemented which allows test authors to
focus completely on writing their tests.
For further details on the Criterion library, please consult its documentation.

The compilation with any of the common C compilers is straight forward and
just requires the addition of the library to the link flags
(\texttt{-l criterion}). For example, to compile the above test with the
implementation of the tested function using the \texttt{clang} compiler, type

\begin{lstlisting}[language=bash]
clang sum_to.c sum_to_test.c -o run_tests -l criterion
\end{lstlisting}

This creates an executable that runs the defined tests against the provided
implementation. Thanks to Criterion, this executable offers many additional
features out of the box.
The command-line flags and options can be consulted using the \texttt{--help}
flag. For example, it is
possible to list all available tests or limit the execution to a desired
subset of the available tests.

\begin{lstlisting}[language=bash]
./run_tests --help
./run_tests -l  # list all available tests
./run_tests --filter sum_to/fast_test
\end{lstlisting}

Given the overall simplicity of compiling the tests, using more advanced build
tools like \texttt{make} and \texttt{CMake} to generate the executable files
is straight forward and does not require further explanation.


\subsection{Testing C++ Programs - Catch2}
Similar to C, there are quite many alternative testing frameworks available for
C++. Among the best and most renowned are Catch2 and GoogleTest (also known
as gtest). In this section, Catch2 version 3 will be introduced - which is
also used by Exercism.

Catch2 requires a modern C++ compiler that supports at least C++14. This is
the case for \texttt{g++} and \texttt{clang++} which both also support the
newer C++17 and C++20.

There are different ways of including and using Catch2. It is important to note
that all current approaches have recently changed in version 3 which means
that artificial intelligence sites like bard and GPT will generally not be
helpful in the near future. In addition, no current version of catch is
available as Debian package (at the time of this writing, Debian offers version
2.13 while version 3.5 is the newest release).

To test C++ code with Catch2, one can
either use two downloadable files \texttt{catch\_amalgamated.hpp} and
\texttt{catch\_amalgamated.cpp} which are available on the project's release
page\footnote{\url{https://github.com/catchorg/Catch2/releases}}.
When following this
approach, compilation can be done on the command-line with any modern compiler.
For example

\begin{lstlisting}[language=bash]
clang++ --std=c++17 sum_to.cpp \
  sum_to_test.cpp catch_amalgamated.cpp -o run_tests
\end{lstlisting}

To avoid recompiling the Catch2 library, it would be better to
compile it once and then link against it after changing your programs. This
is usually done with any build tool but can also be accomplished on the
command-line

\begin{lstlisting}[language=bash]
clang++ -c --std=c++17 sum_to.cpp
clang++ -c --std=c++17 sum_to_test.cpp
clang++ -c --std=c++17 catch_amalgamated.cpp
clang++ -o run_tests sum_to.o test_sum_to.o catch_amalgamated.o
\end{lstlisting}

Alternatively, one can use Catch2 as a proper (static) library and
use to piecewise headers. This latter approach is recommended.
When using Catch2 as a library, one should resort to a proper build
tool. At the time of this writing, \texttt{CMake} is one of the most popular
build tools. It works by automatically generating build files for the current
platform, therefore harnessing the power of other build tools like GNU Make.
Catch2 is very well supported by \texttt{CMake} and a sample build
configuration that automatically makes Catch2 available will be shown later in
this subsection. A Catch2-based implementation of the test cases defined in
subsection \ref{ssect:assignment} is in the listed below.

\lstinputlisting[language=c++]{code/sum_to_test.cpp}

Aside of the macros used for writing tests with Catch2, the code is completely
self-explaining for seasoned C++ programmers.
The key assertions are
\texttt{REQUIRE} and \texttt{CHECK}. If the former fails, the current
test fails immediately and no further assertions will be executed. However,
if \texttt{CHECK} fails, the current test will fail, but continue its
execution.
As stated above, Catch2 is very well supported by \texttt{CMake}. Below is
a \texttt{CMakeLists.txt} that automatically downloads Catch2, compiles it
correctly and makes the library available in the project. When using
\texttt{CMake}, it is also possible to mark tests as such. The benefit is that
all test cases can be automatically executed together using the \texttt{ctest}
companion tool.

\lstinputlisting{code/CMakeLists.txt}

The \texttt{add\_executable} directive adds the \texttt{run\_tests} executable
to the project using the specified source files. Don't forget to list both the
file being tested and the file containing the tests.
\texttt{target\_link\_libraries} ensures that this executable is linked against
Catch2. \texttt{add\_test} is optional and allows using \texttt{ctest} to
discover and run this test.
A detailed explanation of all of \texttt{CMake}'s features
is beyond the scope of this paper. The interested user may consult
the project's documentation available at
\url{https://cmake.org/documentation/}.

With all files in place, the test executable can be created. This is usually
done in a separate \texttt{build} directory.

\begin{lstlisting}[language=bash]
mkdir build && cd build
cmake ..
make -j8  # use 8 processor cores when compiling
\end{lstlisting}

Once the compilation is complete, the newly created executable can run
the defined tests against the provided implementation. Thanks to
\texttt{Catch2}, this executable offers
many additional features out of the box. The
command-line flags and options can be consulted using the \texttt{-–help} flag.
For example,
it is possible to list all available tests or limit the execution to a desired
subset of
the available tests.

\begin{lstlisting}[language=bash]
./run_tests --help
./run_tests --list-tests  # list all available tests
./run_tests [sum_to]  # or ./run_tests "sum_to must calculate correctly"
./run_tests [sum_to] --section "corner cases"
\end{lstlisting}


Comprehensive documentation can be found in Catch2's reference documentation
\footnote{\url{https://github.com/catchorg/Catch2/tree/devel/docs}}.
The authors of \texttt{Catch2} also provided examples available at
\url{https://github.com/catchorg/Catch2/tree/devel/examples}
which are helpful when getting started.
