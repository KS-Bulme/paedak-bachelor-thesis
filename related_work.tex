\section{Related Work}
\label{sect:related_work}

A myriad of recent academic publications is dedicated towards automated
grading and the learning theory supporting its advent. In addition, a series
of commercial and non-commercial agents use the approach for teaching computer
programming.

\subsection{Literature Review}
A full literature review related to automated grading in education would
go beyond the scope of a bachelor's thesis. Below is hence a hand picked
selection of recent publications related to the topic.

Particularly relevant prior work has been done by \citet{Kummer:2020} who
investigated automated assessment of code-based assignments in  C and C++
in an advanced vocational school. A key finding is that automated
assessment, in the perception of students, can lead to both positive and
negative changes. Positive changes include immediate feedback, motivation, and
critical engagement. Negative changes include reduced collaboration with peers.
As expected, the study shows that automated assessment and feedback can be used
to accurately assess student learning outcomes. There is no significant
difference in grades between traditional and automated assessment.
\citet{Kummer:2020} also confirmed that automated assessment can reduce the
workload of teachers after the initial effort of creating automated tasks.

\citet{MoserEtAl:2021} assessed the digital competencies of students in a
teacher training program to investigate the impact of distance learning
elements on their professional competencies. The results showed that students
had varying levels of digital competencies at the beginning of their studies,
and that distance learning elements had a positive impact on their professional
competencies. Their results are relevant to this paper as this paper makes
very strong assumptions on the competencies of fellow teachers. First and
foremost, teachers will have to be willing to invest time in further training
to get competent with the tools demonstrated in the remainder of this work.

\citet{FischerEtAl:2020} deals with excessively long school closures which
created additional needs for individual support. The article discusses
self-regulated learning as a valuable tool for supporting students
individually. It highlights the importance of learning strategies and provides
a comprehensive overview of measures to support students in schools. The
authors present concrete strategies for implementing self-regulated learning
in both classroom and distance learning environments. These were particularly
relevant given the heavy impact of the challenges caused by the COVID-19
measures imposed by the German government.
Among the described learning strategies, automated feedback on programming
assignments best fits the external resource-based strategy.

A publication very closely related to this one reviewed the research on
effective training for future IT specialists. \citet{ZinovievaEtAl:2021} found
that online programming simulators offer a promising approach for distance
learning in Ukrainian universities. These simulators can help students develop
professional competencies through practical coding and problem-solving
exercises. Gamification of the learning process can further enhance cognitive
engagement and the overall quality of education. This is relevant as all the
reviewed platforms are built upon unit testing technology that is essentially
identical to the approach presented in this paper. However, their approach does
not employ the git version control system.


\cite{HofmannEtAl:2020} presents and evaluates the Assistive Course Creation
and Evaluation Student Submission tool (ACCESS), which was developed as open
source software during their master studies in computer science
at University of Zuerich UZH. It was initially written to allow around 450
students to instantly receive grades for their Python programming exercises.
The presented approach is an interesting alternative to GitHub classroom and
is, like all other promising approaches encountered during the literature
research, also based on unit tests. Again, ACCESS does not impose the
additional cognitive difficulty of using git but also does not offer them an
applied way to learn git while learning programming.

Finally, another master's thesis by \cite{Maneva:2022} aims to evaluate the use
of automatic grading in teaching computer programming in vocational training
programs in Catalonia and Spain. This is particularly interesting as they are
aiming at the same level of students. The authors surveyed teachers and
students to gather information about their experiences with different automatic
grading systems. Interestingly, their study indicates that students who use
these systems are less motivated to complete programming challenges. This
means that employing such systems does not automatically increase motivation
but may actually demotivate students. Hence, extra care will have to be taken
when systematically starting automated grading in a school.


\subsection{Available Online Systems}
This section briefly introduces a series of online systems that offer automated
evaluation of programming assignments. Each of the systems is based on
unit testing the code learners have to create.

As there is an ever increasing number of similar sites, a selection of
currently most popular services is limited to Hackerrank, Codewars, Exercism,
ACCESS and GH Classroom.

The two commercial services
Hackerrank\footnote{\url{https://www.hackerrank.com/}} and
Codewars\footnote{\url{https://www.codewars.com/}} offer completely free
programming exercises. Both motivate developers with
a gamification approach and virtual rewards. Their business model is
geared towards improving the developer assessment process of IT companies
when hiring new talent. Both services offer a considerable number of
programming languages including C, C++ and Python. While both companies offer
valueable services that can be used to engage students, neither allows free
assessment of a class of students.

Exercism\footnote{\url{https://exercism.org}} offers developer training that is
similar to the services offered by Hackerrank and Codewars. Currently, more
than 60 programming languages can be trained on Exercism. A key difference is
that the service is completely free and open source software and could be
hosted by individual schools or a national educational establishment. A further
considerable advantage is that exercism offers a command-line utility allowing
to solve all of the tasks offline. Limiting online access is hence a
possibility when using this outstanding service. Another unique advantage is
that students may use their favorite development environment and local tools
for larger assignments instead of being limited to a web based editor. In
addition to evaluating correctness of student submissions, Exerecism also
analyzes the code quality and offers valuable feedback.
The content of the site is mostly created by volunteers - educators including
the author of this paper. It is the opinion of the author of this study that
Exercism offers by far the best and most motivating option for studying
computer programming. Students usually require a brief introduction in the
beginning and occasional assistance from teachers later on. While students
may electronically consult for feedback from their teacher, the platform
currently does not offer assessment for an entire class. As the platform uses
open source software licenses, such a feature may be added in the future
though.

ACCESS\footnote{\url{https://access-tool.ch}} is an open source project similar
to Exercism. It benefits from a much smaller community and
is limited to a single programming language at the time of this writing. These
major drawbacks make it inferior to Exercism in all but one category.
ACCESS already allows assessment of an entire class which is a feature
currently missing in Exercism.

Finally, GitHub Classroom\footnote{\url{https://classroom.github.com}}
is currenty the
only service directly based on the git version control system. As the use of
git is a major cognitive difficulty, this could be seen as a drawback. However,
since students are generally expected to be fluent with basic git workflows
when they graduate, this potential drawback can also be seen as an advantage.
GitHub Classroom forces students to actively work with git in a way very
similiar to regular workplace situations. In addition, the platform
uses GitHub actions which provides the most versatile capabilities of all
the introduced platforms. GitHub actions essentially spawn a Linux container
that can install and run any software. This means, it can be used to evaluate
every existing programming language with every existing tool.

A key drawback of GitHub Classroom is that it is all but simple to get started
with. Hence, this paper provides comprehensive examples that will make it
easier for fellow teachers to get started. Another drawback is that GitHub
is a proprietary platform owned by Microsoft. While it is generally beneficial
to teach current standards, teaching a single proprietary implementation owned
by a single company may be seen critically. Alas, no other git hosting site
offers services similar to GitHub Classroom. Hence, it would be a fruitful
project to extend an open source based git hosting platform like
Forgejo\footnote{\url{https://forgejo.org}} with similar capabilities.
An advantage
of creating git based assignments is that porting these assignments to another
hosting service in the future should be almost trivial.

